import React, {useState, useEffect} from 'react';
import {NavLink} from "react-router-dom";
import axiosQuotes from "../../axios-quotes";

const Quote = props => {
    const [quote, setQuote] = useState({});
    const id = props.match.params.id;

    useEffect(() => {
        const fetchData = async () => {
            try{
                const response = await axiosQuotes.get('/quotes/' + id + '.json');

                setQuote(response.data);
            } catch (e) {
                console.error('Network error')
            };
        };

        fetchData().catch(console.error);
    }, [id]);

    const deleteQuote = async () => {
        try {
            await axiosQuotes.delete('/quotes/' + id + '.json');
        } catch (e) {
            console.error('Network error')
        }
    }

    return Object.keys(props.quotes).map(quote => {
        return (
            <div key={quote} className="Quote">
                <p>{props.quotes[quote].text}</p>
                <p>{props.quotes[quote].author}</p>
                <button><NavLink to={'quotes/' + id + '/edit'}>Edit</NavLink></button>
                <button onClick={deleteQuote}>Delete</button>
            </div>
        )
    })
};

export default Quote;