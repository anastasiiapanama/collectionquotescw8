import React, {useEffect} from 'react';
import {useState} from "react/cjs/react.production.min";
import axiosQuotes from "../../axios-quotes";

const FamousPeople = (props) => {
    const [starWars, setStarWars] = useState({});
    const id = props.match.params.id;

    useEffect(() => {
        const fetchData = async () => {
            try{
                const response = await axiosQuotes.get('/quotes.json?orderBy="category"&equalTo=' + id);

                console.log(response)
                setStarWars(response.data);
            } catch (e) {
                console.error('Network error')
            };
        };

        fetchData().catch(console.error);
    }, [id]);

    return (
        <div className="Quote">

        </div>
    );
};

export default FamousPeople;