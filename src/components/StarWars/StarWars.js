import React, {useEffect} from 'react';
import {useState} from "react/cjs/react.production.min";
import axiosQuotes from "../../axios-quotes";

const StarWars = () => {
    const [starWars, setStarWars] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            try{
                const response = await axiosQuotes.get('/quotes.json?orderBy="category"&equalTo="star-wars"');

                setStarWars(response.data);
            } catch (e) {
                console.error('Network error')
            };
        };

        fetchData().catch(console.error);
    }, []);

    return (
        <div className="Quote">
            <p>{starWars.text}</p>
            <p>{starWars.author}</p>
        </div>
    );
};

export default StarWars;