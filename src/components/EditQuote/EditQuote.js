import React, {useState, useEffect} from 'react';
import axiosQuotes from "../../axios-quotes";

const EditQuote = props => {
    const id = props.match.params.id;

    const [editQuote, setEditQuote] = useState({
        author: '',
        category: '',
        text: ''
    });

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axiosQuotes.get('/quotes/' + id + '.json');

                setEditQuote(response.data);
            } catch (e) {
                console.log('Network error');
            }
        };

        fetchData().catch(console.error);
    }, [id]);

    const changeHandler = event => {
        const {name, value} = event.target;

        setEditQuote(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const editHandler = async event => {
        event.preventDefault();

        const editedQuote = {
            author: editQuote.author,
            category: editQuote.category,
            text: editQuote.text
        }

        try {
            await axiosQuotes.put('/quotes/' + id + '.json', editedQuote);
        } finally {
            props.history.push('/');
        }
    };

    return (
        <div className="Quotes-block">
            <h2>Submit new quote</h2>
            <form className="Add-form" onSubmit={editHandler}>
                <h3>Category</h3>
                <select className="Form-select"
                        name='category' onChange={changeHandler}
                >
                    <option defaultValue="star-wars">Star Wars</option>
                    <option value="famous-people">Famous People</option>
                    <option value="saying">Saying</option>
                    <option value="humour">Humour</option>
                    <option value="motivational">Motivational</option>
                </select>

                <h3>Author</h3>
                <input
                    className="input-text"
                    type="text" name="author"
                    value={editQuote.author}
                    onChange={changeHandler}
                />

                <h3>Quote text</h3>
                <input
                    className="input-text"
                    type="text" name="text"
                    value={editQuote.text}
                    onChange={changeHandler}
                />

                <button>Save</button>
            </form>
        </div>
    );
};

export default EditQuote;