import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Quotes from "./containers/Quotes/Quotes";
import AddQuote from "./containers/AddQuote/AddQuote";
import StarWars from "./components/StarWars/StarWars";
import FamousPeople from "./components/FamousPeople/FamousePeople";
import EditQuote from "./components/EditQuote/EditQuote";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Quotes}/>
          <Route path="/add-quote" component={AddQuote}/>
          <Route path="/quotes/:id/edit" exact component={EditQuote} />
          <Route path="/quotes/star-wars" exact component={StarWars} />
          <Route path="/quotes/famous-people" exact component={FamousPeople} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
