import React, {useState} from 'react';
import axiosQuotes from "../../axios-quotes";
import {NavLink} from "react-router-dom";

const AddQuote = props => {
    const [quotes, setQuotes] = useState({
        author: '',
        category: '',
        text: ''
    });

    const quotesDataChanged = event => {
        const {name, value} = event.target;

        setQuotes(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const quotesHandler = async event => {
        event.preventDefault();

        const quotesOne = {
            author: quotes.author,
            category: quotes.category,
            text: quotes.text
        }

        try {
            await axiosQuotes.post('/quotes.json', quotesOne);
        } finally {
            props.history.push('/');
        }
    };

    return (
        <div className="Container">
            <div className="Header-content">
                <h1>Quotes Central</h1>

                <button className="Quote-button"><NavLink to={'/'}>Quotes</NavLink></button>
                <button className="Quote-button"><NavLink to={'add-quote'}>Submit new quote</NavLink></button>
            </div>

            <div className="Quotes-block">
                <h2>Submit new quote</h2>
                <form className="Add-form" onSubmit={quotesHandler}>
                    <h3>Category</h3>
                    <select className="Form-select"
                            name='category' onChange={quotesDataChanged}
                    >
                        <option defaultValue="star-wars">Star Wars</option>
                        <option value="famous-people">Famous People</option>
                        <option value="saying">Saying</option>
                        <option value="humour">Humour</option>
                        <option value="motivational">Motivational</option>
                    </select>

                    <h3>Author</h3>
                    <input
                        className="input-text"
                        type="text" name="author"
                        value={quotes.author}
                        onChange={quotesDataChanged}
                    />

                    <h3>Quote text</h3>
                    <input
                        className="input-text"
                        type="text" name="text"
                        value={quotes.text}
                        onChange={quotesDataChanged}
                    />

                    <button>Save</button>
                </form>
            </div>
        </div>
    );
};

export default AddQuote;