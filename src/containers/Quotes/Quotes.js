import React, {useState, useEffect} from 'react';
import {NavLink} from "react-router-dom";
import axiosQuotes from "../../axios-quotes";
import Quote from "../../components/Quote/Quote";

const Quotes = () => {
    const [quotes, setQuotes] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axiosQuotes.get('/quotes.json');
                console.log(response)
                setQuotes(response.data);
            } catch (e) {
                console.error('Network error');
            }
        };

        fetchData().catch(console.error);
    }, []);

    return (
        <div className="Container">
            <div className="Header-content">
                <h1>Quotes Central</h1>

                <button className="Quote-button"><NavLink to={'/'}>Quotes</NavLink></button>
                <button className="Quote-button"><NavLink to={'add-quote'}>Submit new quote</NavLink></button>
            </div>

            {quotes &&
                <Quote
                    quotes={quotes}
                />
            }

            <div className="Navigation">
                <NavLink to="/quotes/star-wars">Star-wars</NavLink>
                <NavLink to="/quotes/famous-people">Famous People</NavLink>
            </div>
        </div>
    );


};

export default Quotes;