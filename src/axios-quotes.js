import axios from "axios";

const axiosQuotes = axios.create({
   baseURL: 'https://quotes-cw8-ponamareva-default-rtdb.firebaseio.com/'
});

export default axiosQuotes;